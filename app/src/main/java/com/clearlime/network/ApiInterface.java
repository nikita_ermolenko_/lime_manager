package com.clearlime.network;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.clearlime.util.Const;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by user on 27.06.2015.
 */
public class ApiInterface {
    private RestClient mRestClient;

    public ApiInterface(RestClient client) {
        mRestClient = client;
    }

    public void login(UserCreditionals creditionals, Response.Listener responseListener, Response.ErrorListener errorListener) {
        String json = "";
        try {
            json = new ObjectMapper().writeValueAsString(creditionals);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        JsonRequest request = new JsonRequest(Request.Method.POST, Const.Urls.LOGIN, json, responseListener, errorListener) {

            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject json = new JSONObject(jsonString);
                    Cache.Entry entry = HttpHeaderParser.parseCacheHeaders(response);

                    return Response.success(json, entry);

                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }
        };
        mRestClient.handleSafeRequest(request);
    }

    public void postStatus(int status, String token, Response.Listener responseListener, Response.ErrorListener errorListener) {
        StringRequest request = new StringRequest(Request.Method.POST, String.format(Const.Urls.CHANGE_STATUS, status, token), responseListener, errorListener);
        mRestClient.handleSafeRequest(request);
    }

    public void logout(String token, Response.Listener responseListener, Response.ErrorListener errorListener) {
        String json = "";
        try {
            json = new ObjectMapper().writeValueAsString(token);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        JsonRequest request = new JsonRequest(Request.Method.POST, Const.Urls.LOGIN, json, responseListener, errorListener) {

            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject json = new JSONObject(jsonString);
                    Cache.Entry entry = HttpHeaderParser.parseCacheHeaders(response);

                    return Response.success(json, entry);

                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }
        };
        mRestClient.handleSafeRequest(request);
    }
}
