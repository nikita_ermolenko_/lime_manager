package com.clearlime.network;

/**
 * Created by user on 27.06.2015.
 */
public class UserCreditionals {
    private String login;
    private String pass;

    public UserCreditionals() {
    }

    public UserCreditionals(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String toString() {
        return "UserCreditionals{" +
                "login='" + login + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
