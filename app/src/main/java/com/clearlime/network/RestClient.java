package com.clearlime.network;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

/**
 * Created by user on 27.06.2015.
 */
public class RestClient {
    private RequestQueue mRequestQueue;

    public RestClient(RequestQueue queue) {
        mRequestQueue = queue;
        mRequestQueue.start();
    }

    public void handleSafeRequest(Request request) {
        try {
            mRequestQueue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
