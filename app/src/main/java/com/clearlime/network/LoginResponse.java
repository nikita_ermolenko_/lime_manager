package com.clearlime.network;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by user on 27.06.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {
    private int online;
    private int status;
    private String token;

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "online=" + online +
                ", status=" + status +
                ", token='" + token + '\'' +
                '}';
    }
}
