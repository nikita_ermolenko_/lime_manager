package com.clearlime.ui.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clearlime.R;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.Const;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 13.06.2015.
 */
public class GuideFragment extends Fragment {
    private String mTitle;
    private String mText;
    private int mImage;
    private MainActivity mContext;

    @InjectView(R.id.title)
    TextView mTitleView;
    @InjectView(R.id.text)
    TextView mTextView;
    @InjectView(R.id.image)
    ImageView mImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mTitle = getString(args.getInt(Const.GuideFragmentParams.TITLE));
        mText = getString(args.getInt(Const.GuideFragmentParams.TEXT));
        mImage = args.getInt(Const.GuideFragmentParams.IMAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.guide_fragment, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface regular = Typeface.createFromAsset(mContext.getAssets(), "MyriadProRegular.ttf"),
                 bold = Typeface.createFromAsset(mContext.getAssets(), "MyriadProBold.ttf");
        mTitleView.setTypeface(bold);
        mTextView.setTypeface(regular);
        mTitleView.setText(mTitle);
        mTextView.setText(mText);
        mImageView.setImageResource(mImage);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
