package com.clearlime.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clearlime.R;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.ConnectionManager;
import com.clearlime.util.Const;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 13.06.2015.
 */
public class WebViewFragment extends Fragment {
    @InjectView(R.id.web_view) WebView mWebView;
    @InjectView(R.id.stub_import) ViewStub mProgress;
    private MainActivity mContext;
    private String mUrl;
    private int mSection;
    private CookieManager mCookieManager;
    private ConnectionManager mConnectionManager;
    private Toolbar mToolbar;
    private RelativeLayout mCustom;
    private TextView tableMode;
    private TextView mapMode;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getArguments().getString(Const.WebFragmentParams.URL);
        mSection = getArguments().getInt(Const.WebFragmentParams.SECTION);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.web_fragment, container, false);
        ButterKnife.inject(this, view);
        DrawerLayout drawerLayout = mContext.getDrawer().getDrawerLayout();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mCustom = (RelativeLayout) mToolbar.findViewById(R.id.custom);
        mContext.getSupportActionBar().show();
        if (Const.AVAILABLE_ORDERS == mSection) {
            mCustom.setVisibility(View.VISIBLE);
            tableMode = (TextView) mToolbar.findViewById(R.id.table_mode);
            tableMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setBackgroundResource(R.drawable.selected);
                    ((TextView) v).setTextColor(getResources().getColor(R.color.company_color));
                    TextView mapMode = (TextView) mToolbar.findViewById(R.id.map_mode);
                    mapMode.setBackgroundResource(R.drawable.not_selected);
                    mapMode.setTextColor(Color.WHITE);
                    mWebView.loadUrl("http://clearlime.ru/m/worker/index.php?token=" +
                            mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).getString(Const.TOKEN, ""));
                }
            });
            tableMode.setBackgroundResource(R.drawable.selected);
            tableMode.setTextColor(getResources().getColor(R.color.company_color));
            mapMode = (TextView) mToolbar.findViewById(R.id.map_mode);
            mapMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setBackgroundResource(R.drawable.selected);
                    ((TextView) v).setTextColor(getResources().getColor(R.color.company_color));
                    TextView tableMode = (TextView) mToolbar.findViewById(R.id.table_mode);
                    tableMode.setBackgroundResource(R.drawable.not_selected);
                    tableMode.setTextColor(Color.WHITE);
                    mWebView.loadUrl("http://clearlime.ru/m/worker/index.php?mode=map&token=" +
                    mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).getString(Const.TOKEN, ""));
                }
            });
            mapMode.setBackgroundResource(R.drawable.not_selected);
        } else {
            mCustom.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (Const.AVAILABLE_ORDERS == mSection) {
            setHasOptionsMenu(true);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mContext.getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        ActionBar bar = mContext.getSupportActionBar();
        if (null != bar) {
            bar.show();
        }
        if (mSection == Const.AVAILABLE_ORDERS) {
            mContext.getToolbar().setTitle("");
        }
        if (mConnectionManager.hasInternetConnection()) {
            mCookieManager = CookieManager.getInstance();
            mCookieManager.setAcceptCookie(true);
            mWebView.loadUrl(mUrl);
            WebSettings webSettings = mWebView.getSettings();
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setAppCacheEnabled(true);
            mWebView.getSettings().setDatabaseEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);
            mWebView.getSettings().setGeolocationDatabasePath(mContext.getFilesDir().getPath());
            mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            mWebView.getSettings().setGeolocationEnabled(true);
            mWebView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onGeolocationPermissionsShowPrompt(String origin1,
                                                               GeolocationPermissions.Callback callback1) {
                    final boolean remember = true;
                    final String origin = origin1;
                    final GeolocationPermissions.Callback callback = callback1;
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(R.string.geo);
                    builder.setMessage(R.string.text)
                            .setCancelable(true).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // origin, allow, remember
                            callback.invoke(origin, true, remember);
                        }
                    }).setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // origin, allow, remember
                            callback.invoke(origin, false, remember);
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                public void onProgressChanged(WebView view, int progress) {
                    if (100 == progress) {
                        mProgress.setVisibility(View.GONE);
                    } else {
                        mProgress.setVisibility(View.VISIBLE);
                    }
                }
            });
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            mWebView.loadUrl(mUrl);
        }
        mContext.getDrawer().closeDrawer();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
        mConnectionManager = new ConnectionManager(mContext);
        mToolbar = mContext.getToolbar();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mConnectionManager = null;
        mToolbar = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                mWebView.loadUrl("http://clearlime.ru/m/worker/index.php?mode=filter&token=" +
                        mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).getString(Const.TOKEN, ""));
                break;
        }
        return false;
    }
}
