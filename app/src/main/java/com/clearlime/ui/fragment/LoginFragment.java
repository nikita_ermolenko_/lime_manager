package com.clearlime.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.clearlime.R;
import com.clearlime.network.ApiInterface;
import com.clearlime.network.LoginResponse;
import com.clearlime.network.UserCreditionals;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.Const;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.gc.materialdesign.widgets.Dialog;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by user on 27.06.2015.
 */
public class LoginFragment extends Fragment implements Response.Listener, Response.ErrorListener {
    @InjectView(R.id.login) EditText mLoginInput;
    @InjectView(R.id.pass) EditText mPassInput;
    @InjectView(R.id.login_button) ButtonFlat mLoginButton;
    @InjectView(R.id.progrss_bar) ProgressBarCircularIndeterminate mProgress;
    private MainActivity mContext;
    private ApiInterface mApiInterface;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.login_fragment, container, false);
        ButterKnife.inject(this, view);
        setupUI(view);
        mContext.getSupportActionBar().hide();

        return view;
    }

    View.OnFocusChangeListener mFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.login:
                    String name = mLoginInput.getText().toString();
                    if (name.equals(getString(R.string.login_input_text))) {
                        mLoginInput.setText("");
                        mLoginInput.setTextColor(getResources().getColor(R.color.translucent_gray));
                    } else if (name.equals("")) {
                        mLoginInput.setText(getString(R.string.login_input_text));
                        mLoginInput.setTextColor(getResources().getColor(R.color.translucent_gray));
                    } else {
                        mLoginInput.setTextColor(getResources().getColor(R.color.black_overlay));
                    }
                    break;
                case R.id.pass:
                    String password = mPassInput.getText().toString();
                    if (password.equals(getString(R.string.pass_input_text))) {
                        mPassInput.setText("");
                        mPassInput.setTextColor(getResources().getColor(R.color.translucent_gray));
                    } else if (password.equals("")) {
                        mPassInput.setText(getString(R.string.pass_input_text));
                        mPassInput.setTextColor(getResources().getColor(R.color.translucent_gray));
                    } else {
                        mPassInput.setTextColor(getResources().getColor(R.color.black_overlay));
                    }
                    break;
            }
        }
    };

    public void setupUI(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(mContext);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginInput.setText(getString(R.string.login_input_text));
        mLoginInput.setTextColor(getResources().getColor(R.color.translucent_gray));
        mPassInput.setText(getString(R.string.pass_input_text));
        mPassInput.setTextColor(getResources().getColor(R.color.translucent_gray));
        mPassInput.setOnFocusChangeListener(mFocusListener);
        mLoginInput.setOnFocusChangeListener(mFocusListener);
        DrawerLayout drawerLayout = mContext.getDrawer().getDrawerLayout();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
        mApiInterface = mContext.getApiInterface();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mApiInterface = null;
        mContext = null;
    }

    @OnClick(R.id.login_button)
    public void onLoginCLick(View v) {
        if (null != mApiInterface ) {
            mApiInterface.login(new UserCreditionals(mLoginInput.getText().toString(), mPassInput.getText().toString()), this, this);
            mLoginButton.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {
        JSONObject json = (JSONObject) response;
        LoginResponse loginResponse;
        Dialog dialog;
        try {
            loginResponse = new ObjectMapper().readValue(json.toString(), new TypeReference<LoginResponse>() {});
            int status  = loginResponse.getStatus();
            switch (status) {
                case 0:
                    dialog = new Dialog(mContext, getString(R.string.error), getString(R.string.user_not_exist));
                    dialog.show();
                    dialog.getButtonAccept().setText(getString(R.string.ok));
                    mLoginButton.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).edit()
                            .putString(Const.TOKEN, loginResponse.getToken()).putBoolean(Const.LOGGED_IN, true).apply();
                    InputMethodManager imm = (InputMethodManager)mContext.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mPassInput.getWindowToken(), 0);
                    Fragment fr = new WebViewFragment();
                    Bundle args = new Bundle();
                    args.putString(Const.WebFragmentParams.URL, mContext.mDrawerUrls[0] + loginResponse.getToken());
                    args.putInt(Const.WebFragmentParams.SECTION, 0);
                    fr.setArguments(args);
                    mContext.commitFragment(fr);
                    break;
                case 2:
                    dialog = new Dialog(mContext, getString(R.string.error), getString(R.string.wrong_creditionals));
                    dialog.show();
                    dialog.getButtonAccept().setText(getString(R.string.ok));
                    mLoginButton.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    dialog = new Dialog(mContext, getString(R.string.error), getString(R.string.user_is_blocked));
                    dialog.show();
                    dialog.getButtonAccept().setText(getString(R.string.ok));
                    mLoginButton.setVisibility(View.VISIBLE);
                    break;
                default:
            }
            mProgress.setVisibility(View.GONE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
