package com.clearlime.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.clearlime.R;
import com.clearlime.network.ApiInterface;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.Const;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;

/**
 * Created by user on 28.06.2015.
 */
public class LogoutFragment extends Fragment implements Response.Listener, Response.ErrorListener {
    private MainActivity mContext;
    private ApiInterface mApiInterface;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.logout_fragment, container, false);
        ButterKnife.inject(this, view);
        mContext.getSupportActionBar().hide();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (null != mApiInterface) {
            mApiInterface.logout(mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).getString(Const.TOKEN, ""), this, this);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
        mApiInterface = mContext.getApiInterface();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mApiInterface = null;
        mContext = null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {
        JSONObject json = (JSONObject) response;
        try {
            if (!json.isNull("status") && 0 == json.getInt("status")) {
                mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).edit().putBoolean(Const.LOGGED_IN, false).apply();
                mContext.commitFragment(new LoginFragment());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
