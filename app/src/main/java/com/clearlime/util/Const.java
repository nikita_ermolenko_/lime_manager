package com.clearlime.util;

/**
 * Created by user on 13.06.2015.
 */
public class Const {
    public static String TOKEN = "TOKEN";

    public interface GuideFragmentParams {
        String TITLE = "TITLE";
        String TEXT = "TEXT";
        String IMAGE = "IMAGE";
        String TYPE = "TYPE";
    }

    public interface WebFragmentParams {
        String URL = "URL";
        String SECTION = "SECTION";
    }

    public static final String PREFS = "PREFS";
    public static final String LOGGED_IN = "LOGGED_IN";
    public static final int AVAILABLE_ORDERS = 0;
    public static final int LOGOUT = 4;

    public interface Urls {
        String LOGIN = "http://clearlime.ru/api/ios.auth.php";
        String CHANGE_STATUS = "http://clearlime.ru/m/worker/index.php?mode=online&status=%s&token=%s";
        String LOGOUT = "http://clearlime.ru/api/ios.exit.php";
    }
}
